import Vue from 'vue';
import Router from 'vue-router';
import BootstrapVue from 'bootstrap-vue';
import 'bootstrap-vue/dist/bootstrap-vue.css';
import 'bootstrap/dist/css/bootstrap.css';

import Hello from '@/components/Hello';
import AddOrder from '../components/Orders/AddOrder';
import Orders from '../components/Orders/Orders';
import navbar from '../components/navbar';
import Products from "../components/Products/Products";
import CreateProduct from "../components/Products/CreateProduct";
import CommonReport from "../components/Reports/CommonReport";

Vue.use(Router);
Vue.use(BootstrapVue);
Vue.component('navbar', navbar)

const EventBus = new Vue();
Object.defineProperties(Vue.prototype, {
  $bus: {
    get: function () {
      return EventBus
    }
  }
});

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Orders',
      component: Orders
    },
    {
      path: '/addOrder',
      name: 'AddOrder',
      component: AddOrder
    },
    {
      path: '/addProduct',
      name: 'CreateProduct',
      component: CreateProduct
    },
    {
      path: '/products',
      name: 'Products',
      component: Products
    },
    {
      path: '/commonReport',
      name: 'CommonReport',
      component: CommonReport
    },

  ]
})
